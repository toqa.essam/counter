// ignore_for_file: unnecessary_null_comparison
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Child', () {
    //initialize
    late FlutterDriver driver;
    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test("ABC", () async {
      //"" lma bb2a hgeb el element in the future
      final textcounter = find.byValueKey("counter");
      final Additi = find.byTooltip("Increment");
      await driver.tap(Additi);
      print(await driver.getText(textcounter));
      expect(await driver.getText(textcounter), "1");
    });

    test("dec", () async {
      final decrease = find.byValueKey("counter");
      final sub = find.byValueKey("decrement");
      await driver.tap(sub);
      print(await driver.getText(decrease));
      expect(await driver.getText(decrease), "0");
    });
  });
}
